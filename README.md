UPD 02.06.2020
using pip updated tensorflow to newest version
upgraded keras down to 2.2.5 due to problems with 2.3.1 lstm bug 
(https://stackoverflow.com/questions/58015489/flask-and-keras-model-error-thread-local-object-has-no-attribute-value)
because of that (incompatibility between keras and tensorflow) changed import 

from

from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM

to

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM