# -*- coding: utf-8 -*-
"""
Created on Mon May 11 22:59:48 2020

@author: User
"""

#import mysql.connector
import traceback
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM
import logging
import matplotlib.pylab as plt
from flask import Flask, render_template#, url_for
from sqlalchemy import create_engine
from flask import request
import random
import string

#import pymysql

import pandas as pd
import numpy as np

app=Flask(__name__)

sqlEngine       = create_engine('mysql+pymysql://root:arp123@127.0.0.1:3308/datasets', pool_recycle=3600)

posts=[
       {
        'author':'Corey Schafer',
        'title':'Blog Post 1',
        'content':'First post content',
        'date_posted':'April 13, 1998'
        },
       {
        'author':'Jane Doe',
        'title':'Blog Post 2',
        'content':'Second post content',
        'date_posted':'April 13, 1998'
        }
]

logging.basicConfig(filename="logs.txt",level=logging.DEBUG)

@app.route("/")
@app.route("/home")
def home():


# mydb = mysql.connector.connect(
#   host="localhost",
#   user='root',
#   passwd="arp123",
#   database="datasets"
# )
# mydb = mysql.connector.connect(
#   host='localhost',
#   port=3308,
#   user='root',
#   passwd='arp123',
#   database='datasets'
# )
    try:
        
        
        dbConnection    = sqlEngine.connect()
        select_statement="select DISTINCT `COMPANY NAME` from data_sets ORDER BY `COMPANY NAME`"

        data           = pd.read_sql(select_statement, dbConnection);
#print(select_statement)
        companies=data.values.tolist()

        pd.set_option('display.expand_frame_repr', False)

#print(data)
#print(type(frame)
        return render_template("home.html",companies=companies)
 
    except Exception as ex:
        logging.error(ex)  
        return render_template("error.html")
    finally:
        dbConnection.close()
        
@app.route('/predict', methods=['POST'])
def predict():
    companyName = request.form['company']
    try:
        results={}
        plots={}
        
        
        
        dbConnection    = sqlEngine.connect()
        select_statement="SELECT * from datasets.data_sets WHERE `Company Name` ='"+companyName+"'"

        data           = pd.read_sql(select_statement, dbConnection);
        indexDate=data.set_index(['Date'])
        pd.set_option('display.expand_frame_repr', False)
        
        
        plt.xlabel("Date")
        plt.ylabel("Close Price")
        indexedDataset=pd.DataFrame(indexDate, columns=['Close'])
        plt.plot(indexedDataset)
        
        plot_name=randomString()
        
        file_trend='static\\'+plot_name+'.png'
        plt.savefig(file_trend)
        plt.close()
        plots['trend']=plot_name+'.png'   
        
        
        #arima
        
        #Parse string to datetime type
        data['Date']=pd.to_datetime(data['Date'],infer_datetime_format=True)
        indexDate=data.set_index(['Date'])

        indexedDataset=pd.DataFrame(data, columns=['Close'])
        #Estimating Trend
        indexedDataset_logScale=np.log(indexedDataset)

        datasetLogDiffshifting= indexedDataset_logScale -indexedDataset_logScale.shift()
        #plt.plot(datasetLogDiffshifting)
        datasetLogDiffshifting.dropna(inplace=True)
        
        #ACF and PACF Plots
     #   from statsmodels.tsa.stattools import acf, pacf
        
      #  lag_acf = acf(datasetLogDiffshifting, nlags=20)
      #  lag_pacf =pacf(datasetLogDiffshifting, nlags=20, method='ols')
        
        #ARIMA Model
        from statsmodels.tsa.arima_model import ARIMA
        
        model = ARIMA(indexedDataset_logScale,order=(2,1,2))
        results_ARIMA = model.fit(disp=-1) 
        
        results_ARIMA.plot_predict(1,549)
        plt.legend(loc='best')
        
        ar_arima=[]
        indexedDataset_ar=indexedDataset.to_numpy()
        for ida in indexedDataset_ar:
            ar_arima.append(ida[0])
        
        x=results_ARIMA.forecast(steps=1)
        
        #ar_arima.append(round(np.exp(x[0][0]),2))
        for xx in x[0]:
            ar_arima.append(round(np.exp(xx),2))
        
        
        
        
        results['arima']=decide_bch(ar_arima)
        #print(results['arima'])
        
        results['arima_txt']=signal_to_bch(results['arima'])
        plot_name=randomString()
        
        file_arima='static\\'+plot_name+'.png'
        plt.savefig(file_arima)
        plt.close()
        plots['arima']=plot_name+'.png'
        #-1 sell
        #0 keep
        #1 buy
        
        #lstm
        todataframe = pd.read_sql(select_statement, dbConnection);
        dbConnection.close()
        todataframe['Date'] = pd.to_datetime(todataframe.Date,format='%Y-%m-%d')
        todataframe.index = todataframe['Date']
        #dataframe creation
        day_to_predict=1
        # sort dataframe
        seriesdata = todataframe.sort_index(ascending=True, axis=0)
        # extract date and closing price
        new_seriesdata = pd.DataFrame(index=range(0,len(todataframe)),columns=['Date','Close'])
        # copy values to the new dataframe
        length_of_data=len(seriesdata)
        for i in range(0,length_of_data):
            new_seriesdata['Date'][i] = seriesdata['Date'][i]
            new_seriesdata['Close'][i] = seriesdata['Close'][i]
        #setting the index again
        new_seriesdata.index = new_seriesdata.Date
        new_seriesdata.drop('Date', axis=1, inplace=True)
        #creating train and test sets this comprises the entire data’s present in the dataset
        myseriesdataset = new_seriesdata.values
        len066=int(len(myseriesdataset)*0.8)
        totrain = myseriesdataset[0:len066,:]
        tovalid = myseriesdataset[len066:,:]
        #converting dataset into x_train and y_train
        scalerdata = MinMaxScaler(feature_range=(0, 1))
        scale_data = scalerdata.fit_transform(myseriesdataset)
        x_totrain, y_totrain = [], []
        length_of_totrain=len(totrain)
        for i in range(60,length_of_totrain):
            x_totrain.append(scale_data[i-60:i,0])
            y_totrain.append(scale_data[i,0])
        x_totrain, y_totrain = np.array(x_totrain), np.array(y_totrain)
        x_totrain = np.reshape(x_totrain, (x_totrain.shape[0],x_totrain.shape[1],1))
        #LSTM neural network
        lstm_model = Sequential()
        lstm_model.add(LSTM(units=50, return_sequences=True, input_shape=(x_totrain.shape[1],1)))
        lstm_model.add(LSTM(units=50))
        lstm_model.add(Dense(1))
        lstm_model.compile(loss='mean_squared_error', optimizer='adadelta')
        lstm_model.fit(x_totrain, y_totrain, epochs=10, batch_size=1, verbose=2)
        #predicting next data stock price
        myinputs = new_seriesdata[len(new_seriesdata) - (len(tovalid)+day_to_predict) - 60:].values
        myinputs = myinputs.reshape(-1,1)
        myinputs  = scalerdata.transform(myinputs)
        tostore_test_result = []
        for i in range(60,myinputs.shape[0]):
            tostore_test_result.append(myinputs[i-60:i,0])
        tostore_test_result = np.array(tostore_test_result)
        tostore_test_result = np.reshape(tostore_test_result,(tostore_test_result.shape[0],tostore_test_result.shape[1],1))
        myclosing_priceresult = lstm_model.predict(tostore_test_result)
        myclosing_priceresult = scalerdata.inverse_transform(myclosing_priceresult)

        
        
        ar_lstm=[]
        for tv in myclosing_priceresult:
            ar_lstm.append(tv)
              
        results['lstm']=decide_bch(ar_arima)
        #print(results['arima'])
        
        results['lstm_txt']=signal_to_bch(results['lstm'])
        plot_name=randomString()
        
        file_lstm='static\\'+plot_name+'.png'
        
        plt.figure(figsize=(16,8))
        plt.plot(myclosing_priceresult,color='red', label='Prediction')
        plt.plot(tovalid,color='green', label='Closing')
        plt.legend()
        
        plt.savefig(file_lstm)
        plt.close()
        plots['lstm']=plot_name+'.png'
        
        
        #slow_stochastic
        
        recomendation=calculate_final(results['arima']+results['lstm'])#+results['slst']
        
        return render_template("prediction.html",company=companyName,results=
                               results,plots=plots,recom=recomendation) 
    except Exception:
        logging.error(traceback.format_exc())  
        return render_template("error.html")
    # your code
    # return a response    
    
def signal_to_bch(signal):
    if signal==1:
        return "Buy"
    elif signal==-1:
        return "Sell"
    else:
        return "Keep"
        
def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

@app.route("/about")
def about():
    return render_template("about.html",title='About')

def calculate_final(score):
    res=""
    if score==3:
        res="Strongly Buy"
    elif score>0:
        res="Buy"
    elif score==0:
        res="Keep"
    elif score==-3:
        res="Strongly Sell"
    else:
        res="Sell"
    return res
        

def decide_bch(raw_input):
    
    
    short=20
    long=60
    
    short_ar=raw_input[len(raw_input)-short:]
    long_ar=raw_input[len(raw_input)-long:]
    
    short_ar_minus1=raw_input[len(raw_input)-short-1:]
    long_ar_minus1=raw_input[len(raw_input)-long-1:]


    av_short=np.mean(short_ar)
    av_long=np.mean(long_ar)

    av_short_minus1=np.mean(short_ar_minus1)
    av_long_minus1=np.mean(long_ar_minus1)
    
    gsl=0
    
    if av_short<av_long and av_short_minus1>=av_long_minus1:
        gsl=-1
    elif (av_short>av_long and av_short_minus1<=av_long_minus1):
        gsl=1
    
    return gsl
        
if __name__=='__main__':
    app.run(debug=True)